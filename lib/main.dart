import 'package:flutter/material.dart';

import 'ui/home_page.dart';


void main(){
  //função padrão
  runApp(MaterialApp(
    home: HomePage(),
    theme: ThemeData(hintColor: Colors.blue, primaryColor: Colors.white)
  ));
}

